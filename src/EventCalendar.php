<?php

declare(strict_types=1);

namespace SkadminUtils\EventCalendar;

use App\Components\Grid\TemplateControl;
use DateTimeInterface;
use Skadmin\Translator\Translator;
use SkadminUtils\EventCalendar\Model\DataModel;
use SkadminUtils\EventCalendar\Model\EventCalendarEvent;
use SkadminUtils\ImageStorage\ImageStorage;

use function intval;
use function method_exists;
use function sprintf;

class EventCalendar extends TemplateControl
{
    public const TYPE_RENDER_MONTH_CALENDAR = 1;
    public const TYPE_RENDER                = [self::TYPE_RENDER_MONTH_CALENDAR => 'monthCalendar'];

    /** @var callable[]&callable(EventCalendar , DateTimeInterface , DateTimeInterface ): void[] ; */
    public array $onLoadEvents;

    /** @var ImageStorage */
    private ImageStorage $imageStorage;

    /** @var string */
    private string $template;

    /** @var int|null @persistent */
    public ?int $month = null;

    /** @var int|null @persistent */
    public ?int $year = null;

    public function __construct(int $typeRender, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->imageStorage = $imageStorage;

        if (isset(self::TYPE_RENDER[$typeRender])) {
            $this->template = self::TYPE_RENDER[$typeRender];
        } else {
            $this->template = self::TYPE_RENDER[self::TYPE_RENDER_MONTH_CALENDAR];
        }
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(sprintf('%s/template/%s.latte', __DIR__, $this->template));
        $template->imageStorage = $this->imageStorage;

        $month                    = DataModel::getMonth($this->month, $this->year);
        $template->daysOfCalendar = DataModel::prepareDataForMonth($month);
        $template->monthText      = $month->format('F');
        $template->year           = $month->format('Y');

        [$firstDay, $lastDay] = DataModel::extractFirstAndLastDayFromDataForMonth($template->daysOfCalendar);
        $this->onLoadEvents($this, $firstDay, $lastDay);

        $template->render();
    }

    public function add(EventCalendarEvent $event): void
    {
        if ($event->getDateFrom() > $event->getDateTo()) {
            return;
        }

        $template = $this->getComponentTemplate();

        $dateFrom = clone $event->getDateFrom();
        while ($dateFrom <= $event->getDateTo()) {
            $template->events[$dateFrom->format('Ymd')][] = $event;

            if (! method_exists($dateFrom, 'modify')) {
                continue;
            }

            $dateFrom->modify('+1 day');
        }
        //$template->eventsOverview[] = $event;
    }

    public function clear(): void
    {
        $template         = $this->getComponentTemplate();
        $template->events = [];
        //$template->eventsOverview = [];
    }

    public function handlePrevMonth(): void
    {
        $this->changeMonth('-1 month');
    }

    public function handleNextMonth(): void
    {
        $this->changeMonth('+1 month');
    }

    private function changeMonth(string $modify): void
    {
        $date = DataModel::getMonth($this->month, $this->year);
        $date->modify($modify);

        $this->month = intval($date->format('n'));
        $this->year  = intval($date->format('Y'));

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->payload->url = $this->link('this');
        }

        $this->redrawControl('snipEventCalendarNav');
        $this->redrawControl('snipEventCalendar');
        $this->redrawControl('snipEvents');
    }
}
