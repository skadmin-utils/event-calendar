<?php

declare(strict_types=1);

namespace SkadminUtils\EventCalendar\Model;

use DateTimeInterface;
use Nette\Utils\DateTime;
use Nette\Utils\Html;

use function implode;
use function sprintf;

class EventCalendarEvent
{
    /** @var DateTimeInterface */
    private DateTimeInterface $dateFrom;

    /** @var DateTimeInterface */
    private DateTimeInterface $dateTo;

    /** @var string */
    private string $title;

    /** @var string */
    private string $shortTitle;

    /** @var Html|string */
    private Html|string $content;

    /** @var string|null */
    private ?string $link = null;

    /** @var string|null */
    private ?string $colorBadge = null;

    /** @var string|null */
    private ?string $colorBadgeText = null;

    /**
     * @param Html|string $content
     */
    public function __construct(DateTimeInterface $dateFrom, DateTimeInterface $dateTo, string $title, $content = '', ?string $link = null, ?string $shortTitle = null)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo   = $dateTo;
        $this->title    = $title;
        $this->content  = $content;
        $this->link     = $link;

        $this->shortTitle = $shortTitle ?? $this->title;
    }

    public function getDateFrom(): DateTimeInterface
    {
        return $this->dateFrom;
    }

    public function getDateTo(): DateTimeInterface
    {
        return $this->dateTo;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getShortTitle(): string
    {
        return $this->shortTitle;
    }

    public function getContent(): Html|string
    {
        return $this->content;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function getCollapseClass(): string
    {
        $from = DateTime::createFromFormat('YmdHis', $this->getDateFrom()->format('YmdHis'));

        $class = [];
        while ($from <= $this->getDateTo() && $from instanceof DateTimeInterface) {
            $class[] = sprintf('e%s', $from->format('Ymd'));
            $from->modify('+1 day');
        }

        return implode(' ', $class);
    }

    public function getColorBadge(): ?string
    {
        return $this->colorBadge;
    }

    public function setColorBadge(?string $colorBadge): void
    {
        $this->colorBadge = $colorBadge;
    }

    public function getColorBadgeText(): ?string
    {
        return $this->colorBadgeText;
    }

    public function setColorBadgeText(?string $colorBadgeText): void
    {
        $this->colorBadgeText = $colorBadgeText;
    }
}
