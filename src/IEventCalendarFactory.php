<?php

declare(strict_types=1);

namespace SkadminUtils\EventCalendar;

interface IEventCalendarFactory
{
    public function create(int $typeRender = EventCalendar::TYPE_RENDER_MONTH_CALENDAR): EventCalendar;
}
