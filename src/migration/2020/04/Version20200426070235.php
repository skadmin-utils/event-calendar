<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200426070235 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'event-calendar.prev-month', 'hash' => '3bf2ca650a46797494b5062f1b8d707a', 'module' => 'front', 'language_id' => 1, 'singular' => 'Předchozí měsíc', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.next-month', 'hash' => '22fcce81e4ed555c70c937ae204723ec', 'module' => 'front', 'language_id' => 1, 'singular' => 'Další měsíc', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.april', 'hash' => '87469e0e3e786c4f3a7278c5bddf7e51', 'module' => 'front', 'language_id' => 1, 'singular' => 'Duben', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.mon', 'hash' => '6a38b9d082113b0c6fde57dbcb78cff2', 'module' => 'front', 'language_id' => 1, 'singular' => 'Po', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.tue', 'hash' => '7e621590df08c4c4a847caedf7bc7a02', 'module' => 'front', 'language_id' => 1, 'singular' => 'Út', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.wed', 'hash' => 'dc1a02fe1bbd57879a6dda6e0a700496', 'module' => 'front', 'language_id' => 1, 'singular' => 'St', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.thu', 'hash' => 'fed259528bfcfb5ca54f5776ee474538', 'module' => 'front', 'language_id' => 1, 'singular' => 'Čt', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.fri', 'hash' => 'c1db03ba80e8c7657a9e0b24f713d3ae', 'module' => 'front', 'language_id' => 1, 'singular' => 'Pá', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.sat', 'hash' => '82c4170f6aaace0182adda23a6039c02', 'module' => 'front', 'language_id' => 1, 'singular' => 'So', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.sun', 'hash' => '4f7d1b84633d37c6f9f5411a667243bf', 'module' => 'front', 'language_id' => 1, 'singular' => 'Ne', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.march', 'hash' => '92c0db70ae1f4d48a028c64ec195ff8e', 'module' => 'front', 'language_id' => 1, 'singular' => 'Březen', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.may', 'hash' => 'd72767ff3f7dac572e65428cc8077a16', 'module' => 'front', 'language_id' => 1, 'singular' => 'Květen', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.june', 'hash' => 'b3a7c32b0a2063fa47ab2183ec3d5201', 'module' => 'front', 'language_id' => 1, 'singular' => 'Červen', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.february', 'hash' => '216f6d3a7b287b8a0c1026b3c7ce9466', 'module' => 'front', 'language_id' => 1, 'singular' => 'Únor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.january', 'hash' => '1dfeaa9e24ff18932d98ed634966f2bb', 'module' => 'front', 'language_id' => 1, 'singular' => 'Leden', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.july', 'hash' => '47ecc6b7d5cf2df4209a3d08b7849390', 'module' => 'front', 'language_id' => 1, 'singular' => 'Červenec', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.august', 'hash' => 'e097e026b62fdcea643200a2615b871a', 'module' => 'front', 'language_id' => 1, 'singular' => 'Srpen', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.september', 'hash' => '7cade61d29a675c02efdb7ee31147790', 'module' => 'front', 'language_id' => 1, 'singular' => 'Září', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.october', 'hash' => '456b0335d2602996b14f587a8ba2854a', 'module' => 'front', 'language_id' => 1, 'singular' => 'Říjen', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.november', 'hash' => '073a4558857d19a4d2520bf9ae2ce7b0', 'module' => 'front', 'language_id' => 1, 'singular' => 'Listopad', 'plural1' => '', 'plural2' => ''],
            ['original' => 'event-calendar.december', 'hash' => 'a2058ea13de8ecbe77bf622107519926', 'module' => 'front', 'language_id' => 1, 'singular' => 'Prosinec', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
